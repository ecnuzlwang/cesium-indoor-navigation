const fs = require('fs');

let geojsonStr = fs.readFileSync('indoor_3d_8.geojson');
let indoor_3d_geojson = JSON.parse(geojsonStr);

let features = indoor_3d_geojson.features;
features.forEach((feature)=>{
	feature.geometry.coordinates.forEach((item)=>{
		item.push(8*4+1);
	})
	feature.properties.SHAPE_Leng=feature.properties.SHAPE_Leng/1000;
});
let newfeatures = [];
let levelnum = 8;
let floorheight = 4;
let OBJECTID = features.length;
for(let i=0;i<levelnum;i++){
	for(let j=0;j<features.length;j++){
		let newfeature = JSON.parse(JSON.stringify(features[j]));//Object.assign({},features[j]);
		newfeature.geometry.coordinates.forEach((item)=>{
			item[2]=i*4+1;
		})
		newfeature.properties.Floor=i;
		newfeature.properties.OBJECTID=OBJECTID;
		OBJECTID++;
		newfeatures.push(newfeature);
	}
}
console.log('add floor done');

let indoor_poi_str = fs.readFileSync('PolyU_CoreM_poi_8.json');
let indoor_poi = JSON.parse(indoor_poi_str);

let pois = indoor_poi.features;
let lifts = pois.filter((poi)=>{
	return poi.properties.Type=='Lift';
})
for(let i=0;i<levelnum;i++){
	for(let j=0;j<lifts.length;j++){
		let coords = lifts[j].geometry.coordinates;
		let lift_line = {
			"type" : "Feature",
			"geometry" : {
				"type" : "LineString",
				"coordinates" : [
					[ coords[0], coords[1], i*4+1 ],
					[ coords[0], coords[1], (i+1)*4+1  ]
				]
			},
			"properties" : {
				"OBJECTID" : OBJECTID,
				"Name" : "Lift"+j,
				"Floor" : i,
				"Type" : "Lift",
				"BuildingID" : "PolyU_M",
				"SHAPE_Leng" : 4
			}
		};
		OBJECTID++;
		newfeatures.push(lift_line);
	}
}
console.log('add lifts done');

indoor_3d_geojson.features = indoor_3d_geojson.features.concat(newfeatures);
fs.writeFileSync('indoor_3d.geojson',JSON.stringify(indoor_3d_geojson));

console.log('done');