const fs = require('fs');

let geojsonStr = fs.readFileSync('indoor_3d.geojson');
let indoor_3d_geojson = JSON.parse(geojsonStr);
let poiStr = fs.readFileSync('poi.geojson');
let poigeojson = JSON.parse(poiStr);

let features = indoor_3d_geojson.features;
let pois = poigeojson.features;

let levelnum = 3;
let floorheight = 3;

pois.forEach((poi)=>{
	let con_cord = poi.geometry.coordinates;
	for(let i=0;i<levelnum-1;i++){
		let feature = {
			"type": "Feature",
			"properties": {
				"Id": i+'_to_'+(i+1)+'_'+poi.properties.Id
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [[con_cord[0],
				con_cord[1],
				i*floorheight],
				[con_cord[0],
				con_cord[1],
				(i+1)*floorheight]]
			}
		}
		features.push(feature);
	}
})

fs.writeFileSync('indoor_3d_withlift.geojson',JSON.stringify(indoor_3d_geojson));